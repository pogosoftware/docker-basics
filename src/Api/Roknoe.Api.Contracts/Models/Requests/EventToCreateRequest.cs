using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Roknoe.Api.Contracts.Models.Requests
{
    public class EventToCreateRequest
    {
        public string EventName { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime EntriesTo { get; set; }
        public string Place { get; set; }
        public string Description { get; set; }
        public ICollection<Agenda> Agenda { get; set; }
    }
}