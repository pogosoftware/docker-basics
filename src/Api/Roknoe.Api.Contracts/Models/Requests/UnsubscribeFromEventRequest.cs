namespace Roknoe.Api.Contracts.Models.Requests
{
    public class UnsubscribeFromEventRequest
    {
        public string Email { get; set; }
    }
}