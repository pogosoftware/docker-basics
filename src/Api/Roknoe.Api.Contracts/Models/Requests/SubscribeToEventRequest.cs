namespace Roknoe.Api.Contracts.Models.Requests
{
    public class SubscribeToEventRequest
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}