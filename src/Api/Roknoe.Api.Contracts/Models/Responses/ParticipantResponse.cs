namespace Roknoe.Api.Contracts.Models.Responses
{
    public class ParticipantResponse
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}