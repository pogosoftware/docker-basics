using System;
using System.Threading;
using System.Threading.Tasks;
using Roknoe.Api.Contracts.Models.Requests;

namespace Roknoe.Api.Contracts.Interfaces.Participants
{
    public interface IUnsubscribeFromEventCommand
    {
        Task ExecuteAsync(Guid eventId, UnsubscribeFromEventRequest unsubscribeFromEventRequest,
            CancellationToken cancellationToken = default(CancellationToken));
    }
}