using System;
using System.Threading;
using System.Threading.Tasks;
using Roknoe.Api.Contracts.Models.Requests;

namespace Roknoe.Api.Contracts.Interfaces.Participants
{
    public interface ISubscribeToEventCommand
    {
        Task ExecuteAsync(Guid eventId, SubscribeToEventRequest subscribeToEventRequest, CancellationToken cancellationToken = default(CancellationToken));
    }
}