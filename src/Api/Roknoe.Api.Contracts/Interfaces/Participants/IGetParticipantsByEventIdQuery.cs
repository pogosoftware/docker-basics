using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Roknoe.Api.Contracts.Models.Responses;

namespace Roknoe.Api.Contracts.Interfaces.Participants
{
    public interface IGetParticipantsByEventIdQuery
    {
        Task<ICollection<ParticipantResponse>> ExecuteAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken));
    }
}