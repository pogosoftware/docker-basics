using System.Threading;
using System.Threading.Tasks;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.Api.Contracts.Models.Responses;

namespace Roknoe.Api.Contracts.Interfaces.Events
{
    public interface ICreateEventCommand
    {
        Task<EventResponse> ExecuteAsync(EventToCreateRequest eventToCreateRequest, CancellationToken cancellationToken = default(CancellationToken));
    }
}