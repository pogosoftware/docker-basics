using System;
using System.Threading;
using System.Threading.Tasks;
using Roknoe.Api.Contracts.Models.Responses;

namespace Roknoe.Api.Contracts.Interfaces.Events
{
    public interface IGetEventByIdQuery
    {
        Task<EventResponse> ExecuteAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken));
    }
}