using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Roknoe.Api.Contracts.Models.Responses;

namespace Roknoe.Api.Contracts.Interfaces.Events
{
    public interface IGetAllEventsQuery
    {
        Task<ICollection<EventResponse>> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}