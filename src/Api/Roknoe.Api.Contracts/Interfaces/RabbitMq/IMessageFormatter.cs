namespace Roknoe.Api.Contracts.Interfaces.RabbitMq
{
    public interface IMessageFormatter<TModel, TMessage>
    {
        TMessage Format(TModel model);
    }
}