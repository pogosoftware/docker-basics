using System.Threading.Tasks;

namespace Roknoe.Api.Contracts.Interfaces.RabbitMq
{
    public interface IPublisher<TModel, TMessage>
    {
        Task PublishAsync(TModel Message);
    }
}