using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Roknoe.Api.Contracts.Interfaces.Events;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.Api.Contracts.Models.Responses;
using Roknoe.Api.Filters;

namespace Roknoe.Api.Controllers
{
    [Produces("application/json")]
    public class EventsController : ControllerBase
    {
        private readonly IGetAllEventsQuery _getAllEventsQuery;
        private readonly IGetEventByIdQuery _getEventByIdQuery;
        private readonly ICreateEventCommand _createEventCommand;

        public EventsController(
            IGetAllEventsQuery getAllEventsQuery,
            IGetEventByIdQuery getEventByIdQuery,
            ICreateEventCommand createEventCommand)
        {
            _getAllEventsQuery = getAllEventsQuery;
            _getEventByIdQuery = getEventByIdQuery;
            _createEventCommand = createEventCommand;
        }

        [HttpGet, Route("api/events")]
        [ProducesResponseType(typeof(ICollection<EventResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllEventsAsync() =>
            Ok(await _getAllEventsQuery.ExecuteAsync());

        [HttpGet, Route("api/events/{eventId}")]
        [EnsureEventExists]
        [ProducesResponseType(typeof(EventResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetEventByIdAsync(Guid eventId) =>
            Ok(await _getEventByIdQuery.ExecuteAsync(eventId));

        [HttpPost, Route("api/events")]
        [EnsureModelStateIsValid]
        [ProducesResponseType(typeof(EventResponse), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(EventResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateEventAsync([FromBody]EventToCreateRequest eventToCreateRequest)
        {
            var newEvent = await _createEventCommand.ExecuteAsync(eventToCreateRequest);
            return CreatedAtAction(nameof(GetEventByIdAsync), new { eventId = newEvent.EventId}, newEvent);
        }
    }
}