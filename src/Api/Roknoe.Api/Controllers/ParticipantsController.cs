using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Roknoe.Api.Contracts.Interfaces.Participants;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.Api.Contracts.Models.Responses;
using Roknoe.Api.Filters;

namespace Roknoe.Api.Controllers
{
    [Produces("application/json")]
    [EnsureEventExists]
    public class ParticipantsController : ControllerBase
    {
        private readonly IGetParticipantsByEventIdQuery _getParticipantsByEventIdQuery;
        private readonly ISubscribeToEventCommand _subscribeToEventCommand;
        private readonly IUnsubscribeFromEventCommand _unsubscribeFromEventCommand;

        public ParticipantsController(IGetParticipantsByEventIdQuery getParticipantsByEventIdQuery,
            ISubscribeToEventCommand subscribeToEventCommand,
            IUnsubscribeFromEventCommand unsubscribeFromEventCommand)
        {
            _getParticipantsByEventIdQuery = getParticipantsByEventIdQuery;
            _subscribeToEventCommand = subscribeToEventCommand;
            _unsubscribeFromEventCommand = unsubscribeFromEventCommand;
        }

        [HttpGet, Route("api/events/{eventId}/participants")]
        [ProducesResponseType(typeof(ICollection<ParticipantResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetParticipantsByEventIdQuery(Guid eventId) =>
            Ok(await _getParticipantsByEventIdQuery.ExecuteAsync(eventId));

        [HttpPost, Route("api/events/{eventId}/participants")]
        [EnsureModelStateIsValid]
        [EnsureParticipantDidNotSubscribeForEvent]
        [ProducesResponseType(typeof(NoContentResult), (int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(BadRequestResult), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ConflictResult), (int)HttpStatusCode.Conflict)]
        public async Task<IActionResult> SubscribeToEvent(Guid eventId, [FromBody] SubscribeToEventRequest subscribeToEventRequest)
        {
            await _subscribeToEventCommand.ExecuteAsync(eventId, subscribeToEventRequest);
            return NoContent();
        }

        [HttpDelete, Route("api/events/{eventId}/participants")]
        [EnsureModelStateIsValid]
        [EnsureParticipantHasSubscribeForEvent]
        [ProducesResponseType(typeof(NoContentResult), (int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> UnsubscribeFromEvent(Guid eventId, [FromBody] UnsubscribeFromEventRequest unsubscribeFromEventRequest)
        {
            await _unsubscribeFromEventCommand.ExecuteAsync(eventId, unsubscribeFromEventRequest);
            return NoContent();
        }
    }
}