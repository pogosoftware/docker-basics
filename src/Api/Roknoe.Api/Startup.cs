﻿using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Roknoe.Api.Extensions;
using Roknoe.Database.Extensions;
using Swashbuckle.AspNetCore.Swagger;

namespace Roknoe.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRabbitMq(Configuration.GetRawRabbitConfiguration())
                .AddCustomMappings()
                .AddMongoDb()
                .AddRepositories()
                .AddValidators()
                .AddCommandsAndQueries()
                .AddSwaggerGen(c => {
                    c.SwaggerDoc("v1", new Info { Title = "Event API", Version="v1"});
                })
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMongoDb();
            app.UseCustomMapping();
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Event API V1");
            });
        }
    }
}
