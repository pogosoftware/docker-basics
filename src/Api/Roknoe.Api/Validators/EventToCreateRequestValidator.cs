using FluentValidation;
using Roknoe.Api.Contracts.Models.Requests;

namespace Roknoe.Api.Validators
{
    internal class EventToCreateRequestValidator : AbstractValidator<EventToCreateRequest>
    {
        public EventToCreateRequestValidator()
        {
            RuleFor(x => x.EventName).NotEmpty().NotNull();
            RuleFor(x => x.EventDate).NotNull();
            RuleFor(x => x.EntriesTo).LessThanOrEqualTo(x => x.EventDate).NotNull();
            RuleFor(x => x.Place).NotEmpty().NotNull();
            RuleFor(x => x.Description).NotEmpty().NotNull();
            RuleFor(x => x.Agenda).NotNull();
        }
    }
}