using FluentValidation;
using Roknoe.Api.Contracts.Models.Requests;

namespace Roknoe.Api.Validators
{
    internal class SubscribeToEventRequestValidator : AbstractValidator<SubscribeToEventRequest>
    {
        public SubscribeToEventRequestValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull();
            RuleFor(x => x.Email).NotNull().NotEmpty().EmailAddress();
        }
    }
}