using FluentValidation;
using Roknoe.Api.Contracts.Models.Requests;

namespace Roknoe.Api.Validators
{
    internal class UnsubscribeFromEventRequestValidator : AbstractValidator<UnsubscribeFromEventRequest>
    {
        public UnsubscribeFromEventRequestValidator()
        {
            RuleFor(x => x.Email).NotNull().NotEmpty().EmailAddress();
        }
    }
}