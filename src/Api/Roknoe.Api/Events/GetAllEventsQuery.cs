using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Mapster;
using Roknoe.Api.Contracts.Interfaces.Events;
using Roknoe.Api.Contracts.Models.Responses;
using Roknoe.Database.Contracts.Interfaces.Repositories;

namespace Roknoe.Api.Events
{
    internal class GetAllEventsQuery : IGetAllEventsQuery
    {
        private readonly IEventRepository _eventRepository;
        
        public GetAllEventsQuery(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task<ICollection<EventResponse>> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var events = await _eventRepository.GetAllEventsAsync(cancellationToken);
            return events.Adapt<ICollection<EventResponse>>();
        }
    }
}