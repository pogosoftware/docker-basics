using System.Threading;
using System.Threading.Tasks;
using Mapster;
using Roknoe.Api.Contracts.Interfaces.Events;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.Api.Contracts.Models.Responses;
using Roknoe.Database.Contracts.Interfaces.Repositories;
using Roknoe.Database.Contracts.Models;

namespace Roknoe.Api.Events
{
    internal class CreateEventCommand : ICreateEventCommand
    {
        private readonly IEventRepository _eventRepository;
        
        public CreateEventCommand(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task<EventResponse> ExecuteAsync(EventToCreateRequest eventToCreateRequest, CancellationToken cancellationToken = default(CancellationToken))
        {
            var eventToCreate = eventToCreateRequest.Adapt<Event>();
            await _eventRepository.CreateEventAsync(eventToCreate, cancellationToken);

            return eventToCreate.Adapt<EventResponse>();
        }
    }
}