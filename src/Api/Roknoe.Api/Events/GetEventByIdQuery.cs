using System;
using System.Threading;
using System.Threading.Tasks;
using Mapster;
using Roknoe.Api.Contracts.Interfaces.Events;
using Roknoe.Api.Contracts.Models.Responses;
using Roknoe.Database.Contracts.Interfaces.Repositories;

namespace Roknoe.Api.Events
{
    internal class GetEventByIdQuery : IGetEventByIdQuery
    {
        private readonly IEventRepository _eventRepository;
        
        public GetEventByIdQuery(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task<EventResponse> ExecuteAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var singleEvent = await _eventRepository.GetEventByIdAsync(eventId, cancellationToken);
            return singleEvent.Adapt<EventResponse>();
        }
    }
}