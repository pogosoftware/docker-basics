using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Mapster;
using RawRabbit;
using Roknoe.Api.Contracts.Interfaces.Participants;
using Roknoe.Api.Contracts.Models.Responses;
using Roknoe.Database.Contracts.Interfaces.Repositories;

namespace Roknoe.Api.Participants
{
    internal class GetParticipantsByEventIdQuery : IGetParticipantsByEventIdQuery
    {
        private readonly IEventRepository _eventRepository;
        private readonly IBusClient _busClient;
        
        public GetParticipantsByEventIdQuery(IEventRepository eventRepository,
            IBusClient busClient)
        {
            _eventRepository = eventRepository;
            _busClient = busClient;
        }

        public async Task<ICollection<ParticipantResponse>> ExecuteAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var singleEvent = await _eventRepository.GetEventByIdAsync(eventId, cancellationToken);
            return singleEvent.Participants.Adapt<ICollection<ParticipantResponse>>();
        }
    }
}