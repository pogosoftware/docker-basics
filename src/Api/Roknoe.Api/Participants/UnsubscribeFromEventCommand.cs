using System;
using System.Threading;
using System.Threading.Tasks;
using RawRabbit;
using Roknoe.Api.Contracts.Interfaces.Participants;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.Database.Contracts.Interfaces.Repositories;

namespace Roknoe.Api.Participants
{
    internal class UnsubscribeFromEventCommand : IUnsubscribeFromEventCommand
    {
        private readonly IParticipantRepository _participantRepository;
        private readonly IEventRepository _eventRepository;
        private readonly IBusClient _busClient;
        
        public UnsubscribeFromEventCommand(IParticipantRepository participantRepository,
            IEventRepository eventRepository,
            IBusClient busClient)
        {
            _participantRepository = participantRepository;
            _eventRepository = eventRepository;
            _busClient = busClient;
        }

        public async Task ExecuteAsync(Guid eventId, UnsubscribeFromEventRequest unsubscribeFromEventRequest,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var unsubscriptionEvent = await _eventRepository.GetEventByIdAsync(eventId);
            await _participantRepository.UnsubscribeFromEventAsync(unsubscriptionEvent, unsubscribeFromEventRequest.Email, cancellationToken);
        }
    }
}