using System;
using System.Threading;
using System.Threading.Tasks;
using Mapster;
using RawRabbit;
using Roknoe.Api.Contracts.Interfaces.Participants;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.Database.Contracts.Interfaces.Repositories;
using Roknoe.Database.Contracts.Models;
using Roknoe.EmailService.Contracts.Messages;

namespace Roknoe.Api.Participants
{
    internal class SubscribeToEventCommand : ISubscribeToEventCommand
    {
        private readonly IParticipantRepository _participantRepository;
        private readonly IEventRepository _eventRepository;
        private readonly IBusClient _busClient;
        
        public SubscribeToEventCommand(IParticipantRepository participantRepository,
            IEventRepository eventRepository,
            IBusClient busClient)
        {
            _participantRepository = participantRepository;
            _eventRepository = eventRepository;
            _busClient = busClient;
        }

        public async Task ExecuteAsync(Guid eventId, SubscribeToEventRequest subscribeToEventRequest, CancellationToken cancellationToken = default(CancellationToken))
        {
            var participantToSubscribe = subscribeToEventRequest.Adapt<Participant>();
            var subscriptionEvent = await _eventRepository.GetEventByIdAsync(eventId);
            await _participantRepository.SubscribeToEventAsync(subscriptionEvent, participantToSubscribe, cancellationToken);

            var message = subscriptionEvent.BuildAdapter()
                .AddParameters("name", participantToSubscribe.Name)
                .AddParameters("email", participantToSubscribe.Email)
                .AdaptToType<ParticipantSubscribeToEventMessage>();

            await _busClient.PublishAsync(message);
        }
    }
}