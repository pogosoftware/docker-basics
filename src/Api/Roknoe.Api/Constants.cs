namespace Roknoe.Api
{
    public static class Constants
    {
        public static class RouteKeys
        {
            public static string EventIdRouteKey => "eventId";
            public static string SubscribeToEventRequestRouteKey => "subscribeToEventRequest";
            public static string UnsubscribeToEventRequestRouteKey => "unsubscribeFromEventRequest";
        }
    }
}