using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.Database.Contracts.Interfaces.Repositories;

namespace Roknoe.Api.Filters
{
    internal class EnsureParticipantDidNotSubscribeForEvent : TypeFilterAttribute
    {
        public EnsureParticipantDidNotSubscribeForEvent()
            : base(typeof(EnsureParticipantDidNotSubscribeForEventImpl)) { }

        private class EnsureParticipantDidNotSubscribeForEventImpl : ActionFilterAttribute
        {
            private readonly IParticipantRepository _participantRepository;

            public EnsureParticipantDidNotSubscribeForEventImpl(IParticipantRepository participantRepository)
            {
                _participantRepository = participantRepository;
            }

            public override void OnActionExecuting(ActionExecutingContext context)
            {
                StatusCodeResult result = null;

                context.ActionArguments.TryGetValue(Constants.RouteKeys.EventIdRouteKey, out object eventId);
                context.ActionArguments.TryGetValue(Constants.RouteKeys.SubscribeToEventRequestRouteKey, out object subscribeToEventRequest);

                if (eventId == null || subscribeToEventRequest == null)
                {
                    result = new BadRequestResult();
                }
                else
                {
                    Guid.TryParse(eventId.ToString(), out Guid eventGuidId);
                    var email = (subscribeToEventRequest as SubscribeToEventRequest).Email;                   
                    if (_participantRepository.HasAlreadySubscribeToEventAsync(eventGuidId, email).GetAwaiter().GetResult())
                    {
                        result = new ConflictResult();
                    }
                }

                context.Result = result;
            }
        }
    }
}