using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.Database.Contracts.Interfaces.Repositories;

namespace Roknoe.Api.Filters
{
    internal class EnsureParticipantHasSubscribeForEvent : TypeFilterAttribute
    {
        public EnsureParticipantHasSubscribeForEvent()
            : base(typeof(EnsureParticipantHasSubscribeForEventImpl)) { }

        private class EnsureParticipantHasSubscribeForEventImpl : ActionFilterAttribute
        {
            private readonly IParticipantRepository _participantRepository;

            public EnsureParticipantHasSubscribeForEventImpl(IParticipantRepository participantRepository)
            {
                _participantRepository = participantRepository;
            }

            public override void OnActionExecuting(ActionExecutingContext context)
            {
                StatusCodeResult result = null;

                context.ActionArguments.TryGetValue(Constants.RouteKeys.EventIdRouteKey, out object eventId);
                context.ActionArguments.TryGetValue(Constants.RouteKeys.UnsubscribeToEventRequestRouteKey, out object unsubscribeToEventRequest);

                if (eventId == null || unsubscribeToEventRequest == null)
                {
                    result = new BadRequestResult();
                }
                else
                {
                    Guid.TryParse(eventId.ToString(), out Guid eventGuidId);
                    var email = (unsubscribeToEventRequest as UnsubscribeFromEventRequest).Email;                   
                    if (!_participantRepository.HasAlreadySubscribeToEventAsync(eventGuidId, email).GetAwaiter().GetResult())
                    {
                        result = new BadRequestResult();
                    }
                }

                context.Result = result;
            }
        }
    }
}