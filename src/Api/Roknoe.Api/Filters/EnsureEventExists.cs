using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Roknoe.Database.Contracts.Interfaces.Repositories;

namespace Roknoe.Api.Filters
{
    internal class EnsureEventExists : TypeFilterAttribute
    {
        public EnsureEventExists() : base(typeof(EnsureEventExistsImpl)) { }

        private class EnsureEventExistsImpl : ActionFilterAttribute
        {
            private readonly IEventRepository _eventRepository;

            public EnsureEventExistsImpl(IEventRepository eventRepository)
            {
                _eventRepository = eventRepository;
            }

            public override void OnActionExecuting(ActionExecutingContext context)
            {
                StatusCodeResult result = null;
                context.ActionArguments.TryGetValue(Constants.RouteKeys.EventIdRouteKey, out object eventId);

                if (eventId == null)
                {
                    result = new BadRequestResult();
                }
                else
                {
                    Guid.TryParse(eventId.ToString(), out Guid eventGuidId);
                    if (!_eventRepository.Exists(eventGuidId).GetAwaiter().GetResult())
                    {
                        result = new NotFoundResult();
                    }
                }

                context.Result = result;
            }
        }
    }
}