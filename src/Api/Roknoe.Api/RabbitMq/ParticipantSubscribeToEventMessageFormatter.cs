using Mapster;
using Roknoe.Api.Contracts.Interfaces.RabbitMq;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.EmailService.Contracts.Messages;

namespace Roknoe.Api.RabbitMq
{
    internal class ParticipantSubscribeToEventMessageFormatter : IMessageFormatter<SubscribeToEventRequest, ParticipantSubscribeToEventMessage>
    {
        public ParticipantSubscribeToEventMessage Format(SubscribeToEventRequest participant)
        {
            return participant.Adapt<ParticipantSubscribeToEventMessage>();
        }   
    }
}