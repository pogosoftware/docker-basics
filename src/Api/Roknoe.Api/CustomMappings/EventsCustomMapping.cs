using System;
using System.Collections.ObjectModel;
using Mapster;
using Roknoe.Api.Contracts.Interfaces;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.Api.Contracts.Models.Responses;
using Roknoe.Database.Contracts.Models;

namespace Roknoe.Api.CustomMappings
{
    internal class EventsCustomMapping : ICustomMapping
    {
        public void Initialize()
        {
            TypeAdapterConfig<Event, EventResponse>
                .NewConfig()
                .Map(dest => dest.EventId, src => src.Id);

            TypeAdapterConfig<EventToCreateRequest, Event>
                .NewConfig()
                .Map(dest => dest.CreatedAt, src => DateTime.UtcNow)
                .Map(dest => dest.Participants, src => new Collection<Participant>());
        }
    }
}