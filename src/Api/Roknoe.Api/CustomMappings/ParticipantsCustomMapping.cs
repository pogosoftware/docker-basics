using Mapster;
using Roknoe.Api.Contracts.Interfaces;
using Roknoe.Database.Contracts.Models;
using Roknoe.EmailService.Contracts.Messages;

namespace Roknoe.Api.CustomMappings
{
    internal class ParticipantsCustomMapping : ICustomMapping
    {
        public void Initialize()
        {
            TypeAdapterConfig<Event, ParticipantSubscribeToEventMessage>
                .NewConfig()
                .Map(dest => dest.Name, src => MapContext.Current.Parameters["name"].ToString())
                .Map(dest => dest.Email, src => MapContext.Current.Parameters["email"].ToString());
        }
    }
}