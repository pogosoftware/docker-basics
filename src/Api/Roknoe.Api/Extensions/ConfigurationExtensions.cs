using Microsoft.Extensions.Configuration;
using RawRabbit.Configuration;

namespace Roknoe.Api.Extensions
{
    internal static class ConfigurationExtensions
    {
        private static string rabbitMqSectionName = "RabbitMq";

        public static RawRabbitConfiguration GetRawRabbitConfiguration(this IConfiguration configuration)
        {
            return configuration.GetSection(rabbitMqSectionName).Get<RawRabbitConfiguration>();
        }
    }
}