using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit;
using RawRabbit.Configuration;
using RawRabbit.vNext;
using RawRabbit.vNext.Pipe;
using Roknoe.Api.Contracts.Interfaces;
using Roknoe.Api.Contracts.Interfaces.Events;
using Roknoe.Api.Contracts.Interfaces.Participants;
using Roknoe.Api.Contracts.Interfaces.RabbitMq;
using Roknoe.Api.Contracts.Models.Requests;
using Roknoe.Api.CustomMappings;
using Roknoe.Api.Events;
using Roknoe.Api.Participants;
using Roknoe.Api.RabbitMq;
using Roknoe.Api.Validators;
using Roknoe.EmailService.Contracts.Messages;

namespace Roknoe.Api.Extensions
{
    internal static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRabbitMq(this IServiceCollection services, RawRabbitConfiguration config)
        {
            services.AddRawRabbit(new RawRabbitOptions {
                ClientConfiguration = config,
                Plugins = p => p.UseAttributeRouting()
            });
            services.AddTransient<IMessageFormatter<SubscribeToEventRequest, ParticipantSubscribeToEventMessage>, ParticipantSubscribeToEventMessageFormatter>();
            services.AddTransient(typeof(IPublisher<,>), typeof(Publisher<,>));

            return services;
        }

        public static IServiceCollection AddCustomMappings(this IServiceCollection services)
        {
            services.AddSingleton<ICustomMapping, EventsCustomMapping>();
            services.AddSingleton<ICustomMapping, ParticipantsCustomMapping>();
            
            return services;
        }

        public static IServiceCollection AddValidators(this IServiceCollection services)
        {
            services.AddTransient<IValidator<EventToCreateRequest>, EventToCreateRequestValidator>();
            services.AddTransient<IValidator<SubscribeToEventRequest>, SubscribeToEventRequestValidator>();
            services.AddTransient<IValidator<UnsubscribeFromEventRequest>, UnsubscribeFromEventRequestValidator>();

            return services;
        }

        public static IServiceCollection AddCommandsAndQueries(this IServiceCollection services)
        {
            AddEventCommands(services);
            AddEventQueries(services);
            AddParticipantCommands(services);
            AddParticipantQueries(services);

            return services;
        }

        private static void AddEventCommands(this IServiceCollection services)
        {
            services.AddTransient<ICreateEventCommand, CreateEventCommand>();
        }

        private static void AddEventQueries(this IServiceCollection services)
        {
            services.AddTransient<IGetAllEventsQuery, GetAllEventsQuery>();
            services.AddTransient<IGetEventByIdQuery, GetEventByIdQuery>();
        }

        private static void AddParticipantCommands(this IServiceCollection services)
        {
            services.AddTransient<ISubscribeToEventCommand, SubscribeToEventCommand>();
            services.AddTransient<IUnsubscribeFromEventCommand, UnsubscribeFromEventCommand>();
        }

        private static void AddParticipantQueries(this IServiceCollection services)
        {
            services.AddTransient<IGetParticipantsByEventIdQuery, GetParticipantsByEventIdQuery>();
        }
    }
}