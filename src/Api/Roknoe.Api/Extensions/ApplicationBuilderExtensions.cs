using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Roknoe.Api.Contracts.Interfaces;

namespace Roknoe.Api.Extensions
{
    internal static class ApplicationBuilderExtensions
    {
        public static void UseCustomMapping(this IApplicationBuilder app)
        {
            var customMappings = app.ApplicationServices.GetServices<ICustomMapping>();
            foreach(var customMapping in customMappings)
            {
                customMapping.Initialize();
            }
        }
    }
}