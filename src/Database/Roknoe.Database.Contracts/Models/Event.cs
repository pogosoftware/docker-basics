using System;
using System.Collections.Generic;

namespace Roknoe.Database.Contracts.Models
{
    public class Event
    {
        public Guid Id { get; set; }

        public string EventName { get; set; }

        public DateTime EventDate { get; set; }

        public DateTime CreatedAt { get; set; }

        public string Place { get; set; }

        public DateTime EntriesTo { get; set; }

        public string Description { get; set; }

        public ICollection<Agenda> Agenda { get; set; }

        public ICollection<Participant> Participants { get; set; }
    }
}