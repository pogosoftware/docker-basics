using System;

namespace Roknoe.Database.Contracts.Models
{
    public class Agenda
    {
        public DateTime Timeline { get; set; }

        public string Title { get; set; }
    }
}