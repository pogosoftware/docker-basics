using System;
using System.Threading;
using System.Threading.Tasks;
using Roknoe.Database.Contracts.Models;

namespace Roknoe.Database.Contracts.Interfaces.Repositories
{
    public interface IParticipantRepository
    {
        Task SubscribeToEventAsync(Event subscriptionEvent, Participant participant, CancellationToken cancellationToken = default(CancellationToken));
        Task UnsubscribeFromEventAsync(Event unsubscriptionEvent, string participantEmail, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> HasAlreadySubscribeToEventAsync(Guid eventId, string participantEmail);
    }
}