using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Roknoe.Database.Contracts.Models;

namespace Roknoe.Database.Contracts.Interfaces.Repositories
{
    public interface IEventRepository
    {
        Task<ICollection<Event>> GetAllEventsAsync(CancellationToken cancellationToken = default(CancellationToken));

        Task<Event> GetEventByIdAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken));

        Task CreateEventAsync(Event eventToAdd, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> Exists(Guid eventId);
    }
}