namespace Roknoe.Database.Contracts.Interfaces
{
    public interface IMongoDbConfiguration
    {
        string ConnectionString { get; set; }

        string DatabaseName { get; set; }
    }
}