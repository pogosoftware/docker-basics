using MongoDB.Driver;
using Roknoe.Database.Contracts.Models;

namespace Roknoe.Database.Contracts.Interfaces
{
    public interface IMongoDbContext
    {
        void Initialize();

        IMongoCollection<Event> Events { get; }
    }
}