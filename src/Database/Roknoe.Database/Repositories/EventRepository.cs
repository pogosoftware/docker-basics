using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Roknoe.Database.Contracts.Interfaces;
using Roknoe.Database.Contracts.Interfaces.Repositories;
using Roknoe.Database.Contracts.Models;

[assembly: InternalsVisibleTo("Roknoe.Database.Extensions")]
namespace Roknoe.Database.Repositories
{
    internal class EventRepository : IEventRepository
    {
        private readonly IMongoDbContext _context;

        public EventRepository(IMongoDbContext context)
        {
            _context = context;
        }
        
        public async Task<ICollection<Event>> GetAllEventsAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.Events
                .AsQueryable()
                .OrderByDescending(workshop => workshop.CreatedAt)
                .ToListAsync(cancellationToken);
        }

        public async Task<Event> GetEventByIdAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.Events
                .AsQueryable()
                .Where(x => x.Id.Equals(eventId))
                .SingleOrDefaultAsync(cancellationToken);
        }

        public async Task CreateEventAsync(Event eventToAdd, CancellationToken cancellationToken = default(CancellationToken))
        {
            await _context.Events.InsertOneAsync(eventToAdd, cancellationToken: cancellationToken);
        }

        public async Task<bool> Exists(Guid eventId)
        {
            return await _context.Events
                .AsQueryable()
                .AnyAsync(x => x.Id.Equals(eventId));
        }
    }
}