using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Roknoe.Database.Contracts.Interfaces;
using Roknoe.Database.Contracts.Interfaces.Repositories;
using Roknoe.Database.Contracts.Models;


[assembly: InternalsVisibleTo("Roknoe.Database.Extensions")]
namespace Roknoe.Database.Repositories
{
    internal class ParticipantRepository : IParticipantRepository
    {
        private readonly IMongoDbContext _context;

        public ParticipantRepository(IMongoDbContext context)
        {
            _context = context;
        }

        public async Task SubscribeToEventAsync(Event subscriptionEvent, Participant participant,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            subscriptionEvent.Participants.Add(participant);

            var filter = Builders<Event>.Filter.Eq(singleEvent => singleEvent.Id, subscriptionEvent.Id);
            var update = Builders<Event>.Update.Set(singleEvent => singleEvent.Participants, subscriptionEvent.Participants);
            await _context.Events.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }

        public async Task UnsubscribeFromEventAsync(Event unsubscriptionEvent, string participantEmail,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var participantToRemove = unsubscriptionEvent.Participants
                .Where(participant => participant.Email.Equals(participantEmail)).Single();
            unsubscriptionEvent.Participants.Remove(participantToRemove);
            
            var filter = Builders<Event>.Filter.Eq(singleEvent => singleEvent.Id, unsubscriptionEvent.Id);
            var update = Builders<Event>.Update.Set(singleEvent => singleEvent.Participants, unsubscriptionEvent.Participants);
            await _context.Events.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }  

        public async Task<bool> HasAlreadySubscribeToEventAsync(Guid eventId, string participantEmail)
        {
            return await _context.Events
                .AsQueryable()
                .AnyAsync(singleEvent =>
                    singleEvent.Id.Equals(eventId) && 
                    singleEvent.Participants.Any(participant => 
                        participant.Email.Equals(participantEmail)));
        }
    }
}