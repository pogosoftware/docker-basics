using System;
using System.Runtime.CompilerServices;
using MongoDB.Driver;
using Roknoe.Database.Contracts.Interfaces;
using Roknoe.Database.Contracts.Models;

[assembly: InternalsVisibleTo("Roknoe.Database.Extensions")]
namespace Roknoe.Database
{
    internal class MongoDbContext : IMongoDbContext
    {
        private readonly IMongoDbConfiguration _config;
        private IMongoDatabase _database;
        
        public MongoDbContext(IMongoDbConfiguration config) 
        {
            _config = config;
        }

        public void Initialize()
        {
            try 
            { 
                var mongoClient = new MongoClient(new MongoUrl(_config.ConnectionString)); 
                _database = mongoClient.GetDatabase(_config.DatabaseName); 
            } 
            catch (Exception ex) 
            { 
                throw new Exception("Can not access to db server.", ex); 
            } 
        }

        public IMongoCollection<Event> Events => _database.GetCollection<Event>("Events");
    }
}