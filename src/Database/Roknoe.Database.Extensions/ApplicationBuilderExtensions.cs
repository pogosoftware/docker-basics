using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Roknoe.Database.Contracts.Interfaces;

namespace Roknoe.Database.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseMongoDb(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetService<IMongoDbContext>().Initialize();
        }
    }
}