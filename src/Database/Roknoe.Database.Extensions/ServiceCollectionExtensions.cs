using ImpromptuInterface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Roknoe.Database.Contracts.Interfaces;
using Roknoe.Database.Contracts.Interfaces.Repositories;
using Roknoe.Database.Repositories;

namespace Roknoe.Database.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMongoDb(this IServiceCollection services)
        {
            services.AddSingleton<IMongoDbContext>(c =>
            {
               var config = new
               {
                   ConnectionString = c.GetService<IConfiguration>()["Database:ConnectionString"],
                   DatabaseName = c.GetService<IConfiguration>()["Database:DatabaseName"]
               };

               return new MongoDbContext(config.ActLike<IMongoDbConfiguration>());
            });

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IEventRepository, EventRepository>();
            services.AddTransient<IParticipantRepository, ParticipantRepository>();

            return services;
        }
    }
}