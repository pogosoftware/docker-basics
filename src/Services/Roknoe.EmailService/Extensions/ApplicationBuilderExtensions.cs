using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit;
using Roknoe.EmailService.Contracts.Interfaces;
using Roknoe.EmailService.Contracts.Messages;

namespace Roknoe.EmailService.Extensions
{
    internal static class ApplicationBuilderExtensions
    {
        public static void UseMessageHandlers(this IApplicationBuilder app)
        {
            var busClient = app.ApplicationServices
                .GetService<IBusClient>();
            var participantWasSignUpHandler = app.ApplicationServices
                .GetService<IMessageHandler<ParticipantSubscribeToEventMessage>>();

            busClient.SubscribeAsync<ParticipantSubscribeToEventMessage>(async message => {
                await participantWasSignUpHandler.HandleAsync(message);
            });
        }
    }
}