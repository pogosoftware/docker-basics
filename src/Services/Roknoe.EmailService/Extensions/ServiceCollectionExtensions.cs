using System;
using ImpromptuInterface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit;
using RawRabbit.Configuration;
using RawRabbit.vNext;
using RawRabbit.vNext.Pipe;
using Roknoe.EmailService.Contracts.Interfaces;
using Roknoe.EmailService.Contracts.Messages;
using Roknoe.EmailService.EmailClients;
using Roknoe.EmailService.MessageHandlers;

namespace Roknoe.EmailService.Extensions
{
    internal static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRabbitMq(this IServiceCollection services, RawRabbitConfiguration config)
        {
            services.AddRawRabbit(new RawRabbitOptions {
                ClientConfiguration = config,
                Plugins = p => p.UseAttributeRouting()
            });

            return services;
        }

        public static IServiceCollection AddEmailClients(this IServiceCollection services)
        {
            services.AddSingleton<IEmailClient>(c =>
            {
               var config = new
               {
                   Host = c.GetService<IConfiguration>()["Email:Host"],
                   Port = Int32.Parse(c.GetService<IConfiguration>()["Email:Port"]),
                   From = c.GetService<IConfiguration>()["Email:From"]
               };

               return new EmailClient(config.ActLike<IEmailConfiguration>());
            });

            return services;
        }

        public static IServiceCollection AddMessageHandlers(this IServiceCollection services)
        {
            services.AddTransient<IMessageHandler<ParticipantSubscribeToEventMessage>, ParticipantSubscribeToEventMessageHandler>();
            
            return services;
        }
    }
}