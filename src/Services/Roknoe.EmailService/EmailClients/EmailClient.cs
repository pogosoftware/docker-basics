using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using Roknoe.EmailService.Contracts.Interfaces;

namespace Roknoe.EmailService.EmailClients
{
    internal class EmailClient : IEmailClient
    {
        private readonly IEmailConfiguration _config;

        public EmailClient(IEmailConfiguration config)
        {
            _config = config;
        }

        public async Task SendEmailAsync<TMessage>(TMessage message)
            where TMessage : MimeMessage
        {
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_config.Host, _config.Port, SecureSocketOptions.None).ConfigureAwait(false);
                await client.SendAsync(message).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
        }
    }
}