using System.Text;
using System.Threading.Tasks;
using MimeKit;
using Roknoe.EmailService.Contracts.Interfaces;
using Roknoe.EmailService.Contracts.Messages;

namespace Roknoe.EmailService.MessageHandlers
{
    internal class ParticipantSubscribeToEventMessageHandler : IMessageHandler<ParticipantSubscribeToEventMessage>
    {
        private readonly IEmailClient _emailClient;

        public ParticipantSubscribeToEventMessageHandler(IEmailClient emailClient)
        {
            _emailClient = emailClient;
        }

        public async Task HandleAsync(ParticipantSubscribeToEventMessage message)
        {
            var messageToSend = new MimeMessage();

            messageToSend.From.Add(new MailboxAddress("Roknoe Events", "events@roknoe.com"));
            messageToSend.To.Add(new MailboxAddress(message.Name, message.Email));
            messageToSend.Subject = "Event details";
            messageToSend.Body = new TextPart("plain") { Text = GetMailBodyText(message) };

            await _emailClient.SendEmailAsync(messageToSend);
        }

        private string GetMailBodyText(ParticipantSubscribeToEventMessage message)
        {
            var sb = new StringBuilder();

            sb.AppendLine($"Hello {message.Name} \n");
            sb.Append("We are pleased to announce that you have successfully subscribed to ");
            sb.Append($"\"{message.EventName}\" ");
            sb.Append($"which will be held on {message.EventDate.ToString("dd MMMM yyyy")} ");
            sb.AppendLine($"in {message.Place}.\n");
            sb.Append("An automatically generated message. Please do not reply to it.");

            return sb.ToString();
        }
    }
}