using System;
using RawRabbit.Configuration.Exchange;
using RawRabbit.Enrichers.Attributes;

namespace Roknoe.EmailService.Contracts.Messages
{
    [Exchange(Name = "roknoe", Type = ExchangeType.Topic)]
    [Queue(Name = "participant.subscribe", MessageTtl = 300, DeadLeterExchange = "dlx", Durable = true)]
    [Routing(RoutingKey = "participant.subscribe")]
    public class ParticipantSubscribeToEventMessage
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string EventName { get; set; }
        public DateTime EventDate { get; set; }
        public string Place { get; set; }
    }
}