namespace Roknoe.EmailService.Contracts.Interfaces
{
    public interface IEmailConfiguration
    {
        string Host { get; set; }
        int Port { get; set; }
        string From { get; set; }
    }
}