using System.Threading.Tasks;

namespace Roknoe.EmailService.Contracts.Interfaces
{
    public interface IMessageHandler<TMessage>
    {
        Task HandleAsync(TMessage message);
    }
}