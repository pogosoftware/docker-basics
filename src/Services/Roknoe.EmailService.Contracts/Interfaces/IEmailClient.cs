using System.Threading.Tasks;
using MimeKit;

namespace Roknoe.EmailService.Contracts.Interfaces
{
    public interface IEmailClient
    {
        Task SendEmailAsync<TMessage>(TMessage message)
            where TMessage : MimeMessage;
    }
}