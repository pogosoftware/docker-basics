# Docker basics

This is a sample application used for docker basics workshops.

# How to run it?

## Pre requirements

1. [Vagrant](https://www.vagrantup.com/downloads.html)
2. [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
3. [Postman](https://www.getpostman.com/)

## Create local environment
```
git clone https://bitbucket.org/pogosoftware/docker-basics
cd docker-basics
vagrant up
```
## Login to the vm
```
vagrant ssh
```
## Run backgroud services
```
docker container run -d -p 27017:27017 mongo:4.1
docker container run -d -p 15672:15672 -p 5672:5672 rabbitmq:3.7.13-management-alpine
docker container run -d -p 8025:8025 -p 1025:1025 mailhog/mailhog
```

## Restore and Build Roknoe Solution
```
cd /vagrant/src/
dotnet restore
dotnet build
```

## Run the Roknoe.Api
```
cd /vagrant/src/
dotnet run --project Api/Roknoe.Api
```

## Run the Roknoe.EmailService
```
cd /vagrant/src/
dotnet run --project Services/Roknoe.EmailService
```